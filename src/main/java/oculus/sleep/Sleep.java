package oculus.sleep;

import java.util.ArrayList;
import java.util.List;

import com.earth2me.essentials.Essentials;
import oculus.sleep.eventlisteners.*;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Sleep extends JavaPlugin {
    public List<Player> playersSleeping = new ArrayList();
    private static Sleep intance = null;
    private static Essentials Essentials;

    public void onEnable() {
        new MetricsLite(this, 7776);
        Sleep.intance = this;

        saveDefaultConfig();
        reloadConfig();

        PluginManager pluginmanager = Bukkit.getPluginManager();

        Essentials = (Essentials) pluginmanager.getPlugin("Essentials");

        pluginmanager.registerEvents(new onBedEnter(), this);
        pluginmanager.registerEvents(new onBedLeave(), this);

        if (getConfig().getBoolean("Updater.Enabled")) {
            pluginmanager.registerEvents(new Updater(), this);
        }
    }

    public static Sleep getInstance() { return Sleep.intance; }

    @Override
    public void onDisable() { Sleep.intance = null; }

    public static void pluginmessage(String msg) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.hasPermission("sleep.notify")) {
                Bukkit.getServer().getConsoleSender().sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
                p.sendMessage("§b[" + getInstance().getDescription().getPrefix() + "] " + msg);
            }
        }
    }

    public static boolean isNight(World world) { return (world.getTime() > 12541 && world.getTime() < 23850); }

    public static boolean isVanished(Player p) {
        for (MetadataValue meta : p.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        if (Essentials != null) {
            return Essentials.getUser(p).isVanished();
        }
        return false;
    }

    public static int getOnlinePlayers() {
        int count = 0;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!isVanished(p)) {
                count++;
            }
        }
        return count;
    }

}
